import React, { Component } from "react";
import { connect } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


import classes from "./Email.module.css";
import * as actionCreators from "../../store/actions/index";
import Spinner from "../UI/Spinner/Spinner";


class Email extends Component {
    state = {
        newMail: {
            to: null,
            subject: null,
            body: null
        }
    }

    componentDidMount() {
        this.props.onFetchFolder();
        this.props.onFetchProfile();
    }

    refreshPage = () => {
        window.location.reload(false);
    }

    onMailInputChange = (event) => {
        const { name, value } = event.target;
        this.setState(prevState => {
            return {
                ...prevState,
                newMail: {
                    ...prevState.newMail,
                    [name]: value
                }
            }
        })

    }

    onNewMailSend = (event) => {
        event.preventDefault();
        const { newMail } = this.state;
        if (newMail.to !== null && newMail.subject !== null && newMail.body != null) {
            this.props.onSendMail(newMail.to, newMail.subject, newMail.body);

        }
    }

    render() {
        const { folders, onFetchSelectedEmail, emails, onSingleMail, singleMail, profile,
            compose, onSetCompose, onDeleteMail } = this.props

        if (this.props.sendMailStatus === 201) {
            toast.success("Message sent");
            this.refreshPage();
        }

        if(this.props.deleteStatus === 200){
            toast.success("Message deleted");
            this.refreshPage();
        }

        return (
            <div>
                {this.props.folders.length !== 0 && profile ?
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-3" style={{padding:"0 7.5px"}}>
                                <div >
                                    <div className="card">
                                        <div className="card-body">
                                            <div>
                                                <div style={{ width: "100px", height: "100px" }}>
                                                    <img className="rounded-circle " src={profile.avatarUrl} alt="" style={{ width: "100px", height: "100px" }} />
                                                </div>
                                                <div style={{ marginTop: "10px" }}>
                                                    <h6 className="f-w-600">{profile.name}</h6>
                                                    <p>{profile.email}</p>
                                                </div>
                                            </div>
                                            <ul style={{ listStyle: "none", padding: "0" }}>
                                                <li className={classes.Folder}>
                                                    <button className="btn-primary btn-block " style={{ padding: "5px 10px" }}
                                                        onClick={onSetCompose}
                                                    > NEW MESSAGE</button>
                                                </li>
                                                <li className={classes.Folder} >
                                                    <button className={classes.FolderButton} onClick={() => onFetchSelectedEmail(folders[0].id)}>
                                                        <span className="title">
                                                            {this.props.folders[0].titile}
                                                        </span>
                                                    </button>
                                                </li>
                                                <li className={classes.Folder} >
                                                    <button className={classes.FolderButton} onClick={() => onFetchSelectedEmail(folders[1].id)}>
                                                        <span className="title">
                                                            {this.props.folders[1].titile}
                                                        </span>
                                                    </button>
                                                </li>
                                                <li className={classes.Folder}>
                                                    <button className={classes.FolderButton} onClick={() => onFetchSelectedEmail(folders[2].id)} >
                                                        <span className="title">
                                                            <i className="icon-new-window"></i>
                                                            {this.props.folders[2].titile}
                                                        </span>
                                                    </button>
                                                </li>
                                                <li className={classes.Folder}>
                                                    <button className={classes.FolderButton} onClick={() => onFetchSelectedEmail(folders[3].id)}>
                                                        <span className="title">
                                                            <i className="icon-pencil-alt"></i>
                                                            {this.props.folders[3].titile}
                                                        </span>
                                                    </button>
                                                </li>
                                                <li className={classes.Folder}>
                                                    <button className={classes.FolderButton} onClick={() => onFetchSelectedEmail(folders[4].id)} >
                                                        <span className="title">
                                                            <i className="icon-trash"></i>
                                                            {this.props.folders[4].titile}
                                                        </span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <hr />
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="col-md-4" style={{padding:"0 7.5px"}}>
                                {!this.props.error ?
                                    <div className="card">
                                        <div >
                                            {emails.length !== 0 ?
                                                emails.map(el => {
                                                    return <li key={el.id} className={classes.Emails} onClick={() => onSingleMail(el.id)}>
                                                        <h5>{el.title}</h5>
                                                        <h6>{el.subject}</h6>
                                                        <p style={{ margin: "0" }}>{el.body.substring(0, 50)}...</p>
                                                        <button className={classes.Delete} onClick={() => onDeleteMail(el.id)}>Delete</button>
                                                    </li>
                                                }) : <h2 style={{ padding: "10px" }}>No mail found!</h2>
                                            }
                                        </div>
                                    </div> : <Spinner></Spinner>}
                            </div>

                            <div className="col-md-5" style={{padding:"0 7.5px"}}>
                                {singleMail != null && !compose ?
                                    <div className="card">
                                        <div className={classes.SingleMail}>
                                            <h4>{singleMail.title}</h4>
                                            <h5>{singleMail.subject}</h5>
                                            <p>{singleMail.to}</p>
                                            <p>{singleMail.body}</p>
                                        </div>
                                    </div> : compose && singleMail == null ?
                                        (<div className="card">
                                            <div className={classes.NewMail}>
                                                <div className="row">
                                                    <div className="col-sm-8 xl-50">
                                                        <h4 className="mb-0">New Mail</h4>
                                                    </div>
                                                </div>

                                                <div >
                                                    <form onSubmit={(event) => this.onNewMailSend(event)}>
                                                        <div className="form-group">
                                                            <label className="col-form-label pt-0" >To</label>
                                                            <input className="form-control" type="email"
                                                                name="to" onChange={this.onMailInputChange} />
                                                        </div>
                                                        <div className="form-group">
                                                            <label >Subject</label>
                                                            <input className="form-control" type="text"
                                                                name="subject" onChange={this.onMailInputChange} />
                                                        </div>
                                                        <div className="form-group mb-0">
                                                            <label className="text-muted">Message</label>
                                                            <textarea style={{ display: "block" }} rows="10"
                                                                cols="71" name="body" onChange={this.onMailInputChange}></textarea>
                                                        </div>
                                                        <div className="col-sm-4 xl-50" style={{ marginTop: "15px" }}>
                                                            <button className="btn btn-primary btn-block btn-mail text-center mb-0 mt-0" type="submit"><i className="fa fa-paper-plane mr-2"></i> SEND</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>) : null}
                            </div>
                        </div>
                    </div> : null}
                <ToastContainer></ToastContainer>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.email.profile,
        folders: state.email.folders,
        emails: state.email.emails,
        singleMail: state.email.singleMail,
        compose: state.email.compose,
        error: state.email.error,
        sendMailStatus: state.email.sendMailStatus,
        deleteStatus:state.email.deleteStatus
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchFolder: () => dispatch(actionCreators.folderFetch()),
        onFetchSelectedEmail: (id) => dispatch(actionCreators.selectedEmailFetch(id)),
        onSingleMail: (id) => dispatch(actionCreators.single_mail(id)),
        onFetchProfile: () => dispatch(actionCreators.profileFetch()),
        onSetCompose: () => dispatch(actionCreators.set_compose()),
        onSendMail: (to, subject, body) => dispatch(actionCreators.sendMail(to, subject, body)),
        onDeleteMail: (id) => dispatch(actionCreators.deleteMail(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Email);