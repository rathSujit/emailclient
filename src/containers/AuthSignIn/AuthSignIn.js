import React, { Fragment } from "react";
import { ToastContainer, toast } from "react-toastify";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";

import classes from "./AuthSignIn.module.css";
import * as actionCreators from "../../store/actions/index";
import Spinner from "../UI/Spinner/Spinner";



class AuthSignIn extends React.Component {

    state = {
        username: "",
        password: "",
    }

    inputChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState(prevState => {
            return {
                ...prevState,
                [name]: value
            }
        })
    }

    onLoginFormSubmit = (event) => {
        event.preventDefault();
        const  { username, password } = this.state

        this.props.onAuth(username, password);
        if(!this.props.isAuthenticated){
          toast.error("Please authenticate")
        }

    }
    
    render() {
        const { username, password } = this.state;
        let authRedirect = null;
        if(this.props.isAuthenticated){
          authRedirect = <Redirect to="/mail"></Redirect>
        }

        return (
            <div className={classes.AuthSignIn}>
              {authRedirect}
                <div className={classes.AuthBox}>
                {!this.props.loading ?
                    <form onSubmit={this.onLoginFormSubmit}>
                        <h4>LOGIN</h4>
                        <input className={classes.Input} type="email" required
                            placeholder="Email" name="username" value={username}
                            onChange={this.inputChangeHandler}></input>
                        <input className={classes.Input} type="password" required
                            placeholder="password" name="password" value={password}
                            onChange={this.inputChangeHandler}></input>
                        <button className={classes.Button} type="submit">Login</button>
                    </form>:<Spinner></Spinner>}
                </div>
                <ToastContainer></ToastContainer>
            </div>
        )
    }
}

const mapStateToProps=(state)=>{
  return{
    isAuthenticated : state.auth.token !== null,
    errorMsg:state.auth.errorMsg,
    error:state.auth.error,
    loading:state.auth.loading
  }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        onAuth:(username, password)=>dispatch(actionCreators.auth(username, password))
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(AuthSignIn);