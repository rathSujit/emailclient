import React, { Component } from 'react';
import { Route, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux"

import AuthSignIn from './containers/AuthSignIn/AuthSignIn';
import Email from './containers/Email/Email';
import classes from './App.module.css';
import * as actionCreators from "./store/actions/index";


class App extends Component {

  componentDidMount() {
    this.props.onAutoLogin();
  }

  render() {
    return (
      <div className={classes.App}>
        <Route path="/" exact component={AuthSignIn}></Route>
        {localStorage.getItem("isLogged") === "abc" ?
          <Route path="/mail" component={Email}></Route> : <Redirect to="/" />}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAutoLogin: () => dispatch(actionCreators.authCheck())
  }
}

export default withRouter(connect(null, mapDispatchToProps)(App));
