import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";


import authReducer from "./store/reducers/authReducer";
import emailReducer from "./store/reducers/emailReducer";
const reducer = combineReducers({
  auth:authReducer,
  email:emailReducer
})

const composeEnhancers = window._REDUX_DEVTOOLS_EXTENSION_COMPOSE__|| compose;

const store = createStore(reducer,composeEnhancers(applyMiddleware(thunk)) );

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App></App>
    </BrowserRouter>
  </Provider>

)

ReactDOM.render(

  app
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();