import * as actionTypes from "../actions/actionTypes";


const initialState = {
    allMails: [],
    profile: null,
    folders: [],
    emails: [],
    singleMail: null,
    compose: false,
    loading: false,
    error: false,
    deleteStatus:null,
    sendMailStatus:null
}

const emailReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PROFILE_START:
            return {
                ...state,
                loading: true,
                error: false
            }

        case actionTypes.FETCH_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.payload,
                loading: false,
                error: false
            }

        case actionTypes.FETCH_PROFILE_FAIL:
            return {
                ...state,
                error: true,
                loading: false
            }

        case (actionTypes.FETCH_FOLDERS_START):
            return {
                ...state,
                loading: true,
                error: false
            }
        case (actionTypes.FETCH_FOLDERS_SUCCESS):
            return {
                ...state,
                folders: action.payload,
                loading: false,
                error: false
            }

        case (actionTypes.FETCH_FOLDERS_FAIL):
            return {
                ...state,
                loading: false,
                error: true
            }

        case (actionTypes.FETCH_SELECTED_EMAIL_START):
            return {
                ...state,
                loading: true,
                compose: false,
                error: false
            }
        case (actionTypes.FETCH_SELECTED_EMAIL_SUCCESS):
            return {
                ...state,
                emails: action.payload,
                compose: false,
                error: false
            }

        case (actionTypes.FETCH_SELECTED_EMAIL_FAIL):
            return {
                ...state,
                loading: false,
                compose: false,
                error: true
            }

        case (actionTypes.SINGLE_MAIL):
            const foundMail = state.emails.find(el => {
                return el.id === action.payload
            })
            return {
                ...state,
                singleMail: foundMail,
                compose: false,
                error: false
            }

        case (actionTypes.SET_COMPOSE):
            return {
                ...state,
                compose: !state.compose,
                singleMail: null,
                error: false
            }

        case (actionTypes.SEND_NEW_MAIL_START):
            return {
                ...state,
                loading: true,
                error: false
            }

        case (actionTypes.SEND_NEW_MAIL_SUCCESS):
            return {
                ...state,
                loading: false,
                error: false,
                sendMailStatus:action.payload
            }
        case (actionTypes.SEND_NEW_MAIL_FAIL):
            return {
                ...state,
                loading: false,
                error: true
            }

        case (actionTypes.FETCH_ALL_MAILS_START):
            return {
                ...state,
                loading: true,
                error: false
            }

        case (actionTypes.FETCH_ALL_MAILS_SUCCESS):
            return {
                ...state,
                allMails: action.payload,
                error: false
            }

        case (actionTypes.FETCH_ALL_MAILS_FAIL):
            return {
                ...state,
                loading: false,
                error: true
            }

        case (actionTypes.DELETE_MAIL_START):
            return {
                ...state,
                loading: true,
                error: false
            }

        case (actionTypes.DELETE_MAIL_SUCCESS):
            return{
                ...state,
                loading:false,
                error:false,
                deleteStatus:action.payload
            }

        case (actionTypes.DELETE_MAIL_FAIL):
            return{
                ...state,
                loading:false,
                error:true
            }

        default:
            return state;
    }
}

export default emailReducer;