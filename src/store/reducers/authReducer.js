import * as actionTypes from "../actions/actionTypes";

const initialState = {
    token : null,
    error : false,
    loading :false,
    errorMsg : null

}

const authReducer = (state=initialState, action)=>{
    switch (action.type){
        case actionTypes.AUTH_START:
            return{
                ...state,
                error:null,
                loading:true
            }

        case actionTypes.AUTH_SUCCESS:
            return{
                ...state,
                token:action.payload,
                error:false,
                loading:false,
            }

        case actionTypes.AUTH_FAIL:
            return{
                ...state,
                error:true,
                errorMsg:action.payload,
                loading:false
            }

        default :
            return state;
    }
    
}

export default authReducer;