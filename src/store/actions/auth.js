import * as actionTypes from "./actionTypes";
import axios from "axios";

export const auth_start = () =>{
    return{
        type:actionTypes.AUTH_START
    }
}

export const auth_success = (token) =>{
    return{
        type:actionTypes.AUTH_SUCCESS,
        payload:token
    }
}

export const auth_fail = (error) =>{
    return{
        type:actionTypes.AUTH_FAIL,
        payload:error
    }
}

export const auth = (username, password )=>{
    return dispatch =>{
        dispatch (auth_start());
        const authData = {
            username : username,
            password : password
        }
        axios.post("http://localhost:8081/login",authData).then(response=>{
            localStorage.setItem("token",`Bearer ${response.data.token}`);
            localStorage.setItem("isLogged","abc")
            dispatch(auth_success(response.data.token))
        }).catch(error=>{
            dispatch(auth_fail("NOT AUTHORIZED"))
            
        });

    }
}

export const authCheck=()=>{
    return dispatch=>{
        const token = localStorage.getItem("token");
        if(token){
            dispatch(auth_success(token))
        }
    }
}