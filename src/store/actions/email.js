import * as actionTypes from "./actionTypes";
import axios from "axios";

export const profileFetch_start = () =>{
    return{
        type:actionTypes.FETCH_PROFILE_START
    }
}

export const profileFetch_success = (profile)=>{
    return{
        type:actionTypes.FETCH_PROFILE_SUCCESS,
        payload:profile
    }
}

export const profileFetch_fail = (error)=>{
    return{
        type:actionTypes.FETCH_PROFILE_FAIL,
        payload:error
    }
}

export const profileFetch = ()=>{
    const token = localStorage.getItem("token");
    return dispatch=>{
        dispatch(profileFetch_start());
        axios.get("http://localhost:8082/profile",{
            headers:{
                authorization : token
            }
        }).then(response=>{
            dispatch(profileFetch_success(response.data))
        }).catch(error=>{
            dispatch(profileFetch_fail(error))
        })

    }
}

export const folderFetch_start = () =>{
    return{
        type:actionTypes.FETCH_FOLDERS_START
    }
}

export const folderFetch_success = (folders) =>{
    return{
        type:actionTypes.FETCH_FOLDERS_SUCCESS,
        payload:folders
    }
}

export const folderFetch_fail = (error) =>{
    return{
        type:actionTypes.FETCH_FOLDERS_FAIL,
        payload:error
    }
}

export const folderFetch = ()=>{
    const token = localStorage.getItem("token");
    return dispatch =>{
        dispatch (folderFetch_start());
        axios.get("http://localhost:8082/folders",{
            headers:{
                authorization : token
            }
        }).then(response=>{
            dispatch(folderFetch_success(response.data))
            
        }).catch(error=>{
            dispatch(folderFetch_fail(error))
        });

    }
}   


export const selectedEmailFetch_start = ()=>{
    return{
        type:actionTypes.FETCH_SELECTED_EMAIL_START
    }

}

export const selectedEmailFetch_success=(emails)=>{
    return{
        type:actionTypes.FETCH_SELECTED_EMAIL_SUCCESS,
        payload:emails
    }
}

export const selectedEmailFetch_fail = (error)=>{
    return{
        type:actionTypes.FETCH_SELECTED_EMAIL_FAIL,
        payload:error
    }
}

export const selectedEmailFetch = (id)=>{
    const token = localStorage.getItem("token");
    return dispatch=>{
        dispatch(selectedEmailFetch_start());
        axios.get(`http://localhost:8082/folders/${id}/emails`,{
            headers:{
                authorization: token
            }
        }).then(response=>{
            dispatch(selectedEmailFetch_success(response.data));
        }).catch(error=>{
            dispatch(selectedEmailFetch_fail(error))
        })

    }

}

export const single_mail = (id)=>{
    return{
        type:actionTypes.SINGLE_MAIL,
        payload:id
    }

}

export const set_compose = ()=>{
    return{
        type:actionTypes.SET_COMPOSE
    }
}

export const sendMail_start = ()=>{
    return{
        type:actionTypes.SEND_NEW_MAIL_START
    }
}

export const sendMail_succcess = (response) =>{
    return{
        type:actionTypes.SEND_NEW_MAIL_SUCCESS,
        payload:response
    }
}

export const sendMail_fail = (error) =>{
    return{
        type:actionTypes.SEND_NEW_MAIL_FAIL,
        payload:error
    }
}

export const sendMail = (to,subject,body) =>{
    const token = localStorage.getItem("token");
    const postData = {
        to,
        subject,
        body,  
    }
    return dispatch=>{
        dispatch(sendMail_start());
        axios.post("http://localhost:8082/emails",postData,{
            headers:{
                authorization: token
            } 
        }).then(response=>{
            console.log(response);
            dispatch(sendMail_succcess(response.status));
        }).catch(error=>{
            dispatch(sendMail_fail(error))
        })
    }

}


export const fetchAllMails_start = ()=>{
    return{
        type:actionTypes.FETCH_ALL_MAILS_START
    }
}

export const fetchAllMails_success = (response)=>{
    return{
        type:actionTypes.FETCH_ALL_MAILS_SUCCESS,
        payload:response
    }
}

export const fetchAllMails_fail = (error)=>{
    return{
        type:actionTypes.FETCH_ALL_MAILS_FAIL,
        payload:error
    }
}

export const fetchAllMails = ()=>{
    const token = localStorage.getItem("token");
    return dispatch=>{
        dispatch(fetchAllMails_start());
        axios.get("http://localhost:8082/emails",{
            headers:{
                authorization: token
            } 
        }).then(response=>{
            dispatch(fetchAllMails_success(response))
        }).catch(error=>{
            dispatch(fetchAllMails_fail(error))
        })
    }
    
}

export const deleteMail_start = () =>{
    return{
        type:actionTypes.DELETE_MAIL_START
    }
}

export const deleteMail_success = (response) =>{
    return{
        type:actionTypes.DELETE_MAIL_SUCCESS,
        payload:response
    }
}

export const deleteMail_fail = (error) =>{
    return{
        type:actionTypes.DELETE_MAIL_FAIL,
        payload:error
    }
}

export const deleteMail = (id) =>{
    const token = localStorage.getItem("token");
    return dispatch=>{
        dispatch(deleteMail_start());
        axios.delete(`http://localhost:8082/emails/${id}`,{
            headers:{
                authorization: token
            }
        }).then(response=>{
            console.log(response);
            dispatch(deleteMail_success(response.status));
        }).catch(error=>{
            dispatch(deleteMail_fail(error))
        })
    }
    
}